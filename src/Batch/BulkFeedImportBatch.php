<?php

namespace Drupal\bulk_import_feed_links\Batch;

// @codingStandardsIgnoreStart
// Node can be used later to actually create nodes. See commented code block
// in csvimportImportLine() below. Since it's unused right now, we hide it from
// coding standards linting.

use Drupal\feeds\Entity\Feed;
use Drupal\Component\Utility\Html;

class BulkFeedImportBatch {

  public static function createFeedEntity($line, $feed_type, &$context){
    $context['results']['rows_imported']++;
    $line = array_map('base64_decode', $line);
    $context['message'] = t('Importing %title', ['%title' => $line[0]]);
    $feed_exists = \Drupal::entityTypeManager()
      ->getStorage('feeds_feed')
      ->loadByProperties([
        'source' => $line[1],
        'type' => $feed_type,
      ]);
    if (empty($feed_exists)) {
      $feed = Feed::create([
        'title' => Html::decodeEntities($line[0]),
        'type' => $feed_type,
        'source' => $line[1],
      ]);
      $feed->save();
      $feed->import();
    }
  }

  function importFeedFinishedCallback($success, $results, $operations) {
    // The 'success' parameter means no fatal PHP errors were detected. All
    // other error management should be handled using 'results'.
    $messenger = \Drupal::messenger();
    if ($success) {
      $messenger->addMessage(t('Created @count feed contents.', [ '@count' => count($results['rows_imported']) ]));
    }
    else {
      $message = t('Finished with an error.');
    }
    $messenger->addStatus($message);
  }
}