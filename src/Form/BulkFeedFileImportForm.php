<?php

namespace Drupal\bulk_import_feed_links\Form;

use Drupal\Core\Form\FormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Component\Utility\UrlHelper;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Implements an FeedFileImportForm form.
 */
class BulkFeedFileImportForm extends FormBase {

  /**
   * Drupal\Core\Entity\EntityTypeManagerInterface definition.
   *
   * @var Drupal\Core\Entity\EntityTypeManagerInterface
   */
  protected $entityTypeManager;

  /**
   * Class constructor.
   */
  public function __construct(EntityTypeManagerInterface $entityTypeManager) {
    $this->entityTypeManager = $entityTypeManager;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    // Instantiates this form class.
    return new static(
      // Load the service required to construct this class.
      $container->get('entity_type.manager')
    );
  }

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'bulk_import_feed_links_file_upload';
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    $feed_types = $this->entityTypeManager->getStorage('feeds_feed_type')->loadMultiple();
    if (empty($feed_types)) {
      $form['message'] = [
        '#markup' => 'There is no feed type with <b>Download from url</b> fetcher',
      ];
    }
    else {
      global $base_url;
      $sample_csv = $base_url . '/' . drupal_get_path('module', 'bulk_import_feed_links') . '/Sample.csv';
      $applicable_feed_types = [];
      foreach ($feed_types as $feed_type) {
        if ($feed_type->getFetcher()->getPluginId() == 'http') {
          $applicable_feed_types[$feed_type->id()] = $feed_type->label();
        }
      }
      $form['feed_type'] = [
        '#type' => 'select',
        '#title' => $this->t('Select the feed type.'),
        '#description' => $this->t('The contents will be imported to this feed type.'),
        '#required' => TRUE,
        '#options' => $applicable_feed_types,
      ];
      $form['feeds_file'] = [
        '#type' => 'managed_file',
        '#title' => $this->t('Upload CSV File'),
        '#description' => $this->t('Please format your CSV file like this before uploading (URL expected in second column): <a href=":link">CSV</a>.', [':link' => $sample_csv]),
        '#required' => TRUE,
        '#upload_validators' => [
          'file_validate_extensions' => ['csv'],
        ],
        '#upload_location' => 'public://bulk_import_feed_links/',
      ];
      $form['actions']['#type'] = 'actions';
      $form['actions']['submit'] = [
        '#type' => 'submit',
        '#value' => $this->t('Import Feeds'),
        '#button_type' => 'primary',
      ];
    }
    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    $batch = [
      'title'            => $this->t('Importing CSV ...'),
      'operations'       => [],
      'init_message'     => $this->t('Commencing'),
      'progress_message' => $this->t('Processed @current out of @total.'),
      'error_message'    => $this->t('An error occurred during processing'),
      'finished'         => 'Drupal\bulk_import_feed_links\Batch\BulkFeedImportBatch::importFeedFinishedCallback',
    ];
    $feeds_file = $form_state->getValue('feeds_file');
    if (isset($feeds_file[0]) && !empty($feeds_file[0])) {
      $file = $this->entityTypeManager->getStorage('file')->load($feeds_file[0]);
      if (($handle = fopen($file->getFileUri(), "r")) !== FALSE) {
        while (($line = fgetcsv($handle)) !== FALSE) {
          // $line is an array of the csv elements
          if (is_array($line)
          && isset($line[0])
          && isset($line[1])
          && UrlHelper::isValid($line[1], TRUE)) {
            $batch['operations'][] = [
              'Drupal\bulk_import_feed_links\Batch\BulkFeedImportBatch::createFeedEntity',
              [
                array_map('base64_encode', $line),
                $form_state->getValue('feed_type'),
              ],
            ];
          }
        }
        fclose($file);
      }
    }
    batch_set($batch);
  }

}
